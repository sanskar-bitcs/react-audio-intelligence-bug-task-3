import Wrapper from './Wrapper';

export const SearchBar = (props) => {
  return (
    <Wrapper>
     <input type="text" {...props} onChange={(e) => { props.onChange(e.target.value) }} />
    </Wrapper>
  );
};

export default SearchBar;
